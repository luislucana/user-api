# Entrega teste técnico - BackEnd
## USER-API

### Teste Técnico - Backend

#### Foram implementadas as seguintes operações:
- Adicionar um novo usuário
- Adicionar uma lista de novos usuários
- Alterar os dados de um usuário
- Remover logicamente um usuário
- Listar todos os usuários inseridos
- (ADICIONAL) Busca de usuário por nome, sobrenome e/ou CPF.

#### Desenvolvimento realizado com Spring Boot, MySQL e rodando no "embedded container" tomcat.

## Disponibilizado na plataforma de serviços cloud da Amazon Web Services.
Para fazer os testes de todas as operações acima, pode-se utilizar este link do Postman (https://www.getpostman.com/collections/5c16c6d38117caf64896), que é uma collection que contém todas as requisições prontas.

Neste link (http://take.ms/A56bY) se encontra uma imagem ilustrando o uso dessa collection no Postman. O servico está rodando na nuvem e pode ser utilizado para testes.

## Descrição das funcionalidades/operações implementadas

## Adicionar um usuário
### [POST] /users/add

Exemplo de requisição:
{
	"nome": "Fulano",
	"sobrenome": "de Tal",
	"cpf": "00000000010",
	"dataNascimento": "15/01/1980",
	"endereco": {
		"logradouro": "Rua das Pedras",
		"numero": "12",
		"complemento": "A",
		"bairro": "Vila Santa Isabel",
		"cidade": "Joinville",
		"estado": "SC",
		"pais": "Brasil"
	},
	"telefones": [ "(31) 9283-2348", "(42) 9283-2349" ],
	"emails": [ "fulanodetal@gmail.com", "fulano.tal@yahoo.com.br", "fdt@outlook.com" ]
}

Exemplo de resposta:
{
    "id": 11,
    "nome": "FULANO",
    "sobrenome": "DE TAL",
    "cpf": "00000000011",
    "dataNascimento": "15/01/1980",
    "endereco": {
        "logradouro": "RUA DAS PEDRAS",
        "numero": "12",
        "complemento": "A",
        "bairro": "VILA SANTA ISABEL",
        "cidade": "JOINVILLE",
        "estado": "SC",
        "pais": "BRASIL"
    },
    "telefones": [
        "(31) 9283-2348",
        "(42) 9283-2349"
    ],
    "emails": [
        "fulanodetal@gmail.com",
        "fulano.tal@yahoo.com.br",
        "fdt@outlook.com"
    ],
    "status": "ATIVO"
}

## Adicionar uma lista de usuários
### [POST] /users/add/all

Exemplo de requisição:
[
{
	"nome": "Luciano",
	"sobrenome": "Martins",
	"cpf": "26101446069",
	"dataNascimento": "14/08/1972",
	"endereco": {
		"logradouro": "Alameda Santos",
		"numero": "104",
		"complemento": null,
		"bairro": "Jardim Paraíso",
		"cidade": "São Paulo",
		"estado": "SP",
		"pais": "Brasil"
	},
	"telefones": [ "(11) 99183-2348" ],
	"emails": [ "joaop@gmail.com"]
},
{
	"nome": "Gustavo",
	"sobrenome": "Morais",
	"cpf": "17586623090",
	"dataNascimento": "01/11/1989",
	"endereco": {
		"logradouro": "Avenida Pedro Álvares Cabral",
		"numero": "1",
		"complemento": "A",
		"bairro": "Cidade Universitária",
		"cidade": "Rio Branco",
		"estado": "AC",
		"pais": "Brasil"
	},
	"telefones": [ "(99) 91168-2340" ],
	"emails": [ "luisalves@gmail.com"]
}
]

Exemplo de Resposta:
[
    {
        "id": 13,
        "nome": "LUCIANO",
        "sobrenome": "MARTINS",
        "cpf": "26101446069",
        "dataNascimento": "14/08/1972",
        "endereco": {
            "logradouro": "ALAMEDA SANTOS",
            "numero": "104",
            "complemento": null,
            "bairro": "JARDIM PARAÍSO",
            "cidade": "SÃO PAULO",
            "estado": "SP",
            "pais": "BRASIL"
        },
        "telefones": [
            "(11) 99183-2348"
        ],
        "emails": [
            "joaop@gmail.com"
        ],
        "status": "ATIVO"
    },
    {
        "id": 15,
        "nome": "GUSTAVO",
        "sobrenome": "MORAIS",
        "cpf": "17586623090",
        "dataNascimento": "01/11/1989",
        "endereco": {
            "logradouro": "AVENIDA PEDRO ÁLVARES CABRAL",
            "numero": "1",
            "complemento": "A",
            "bairro": "CIDADE UNIVERSITÁRIA",
            "cidade": "RIO BRANCO",
            "estado": "AC",
            "pais": "BRASIL"
        },
        "telefones": [
            "(99) 91168-2340"
        ],
        "emails": [
            "luisalves@gmail.com"
        ],
        "status": "ATIVO"
    }
]

## Obter todos os usuários inseridos
### [GET] /users/all

Exemplo de requisição: (não há necessidade de envio de body)

Exemplo de resposta:
[
    {
        "id": 1,
        "nome": "FULANO",
        "sobrenome": "DE TAL",
        "cpf": "19200391044",
        "dataNascimento": "15/01/1980",
        "endereco": {
            "logradouro": "RUA DAS PEDRAS",
            "numero": "12",
            "complemento": "A",
            "bairro": "VILA SANTA ISABEL",
            "cidade": "JOINVILLE",
            "estado": "SC",
            "pais": "BRASIL"
        },
        "telefones": [
            "(31) 9283-2348",
            "(42) 9283-2349"
        ],
        "emails": [
            "fulanodetal@gmail.com",
            "fulano.tal@yahoo.com.br",
            "fdt@outlook.com"
        ],
        "status": "INATIVO"
    },
    {
        "id": 3,
        "nome": "JOAO",
        "sobrenome": "PEREIRA",
        "cpf": "76101446069",
        "dataNascimento": "14/08/1972",
        "endereco": {
            "logradouro": "ALAMEDA SANTOS",
            "numero": "104",
            "complemento": null,
            "bairro": "JARDIM PARAÍSO",
            "cidade": "SÃO PAULO",
            "estado": "SP",
            "pais": "BRASIL"
        },
        "telefones": [
            "(11) 99183-2348"
        ],
        "emails": [
            "joaop@gmail.com"
        ],
        "status": "ATIVO"
    }
]

## Atualizar os dados de um usuário
### [PUT] /users/{id}

Exemplo de requisição:
{
	"nome": "José",
	"sobrenome": "da Silva",
	"cpf": "00000000010",
	"dataNascimento": "01/04/1922",
	"endereco": {
		"logradouro": "Rua Sem Asfalto",
		"numero": "S/N",
		"complemento": null,
		"bairro": "Jardim Bla",
		"cidade": "Presidente Prudente",
		"estado": "SP",
		"pais": "Brasil"
	},
	"telefones": [ "(18) 9283-2348" ],
	"emails": [ "usuario@provedor.com.br"]
}

Exemplo de resposta:
{
    "id": 17,
    "nome": "JOSÉ",
    "sobrenome": "DA SILVA",
    "cpf": "00000000010",
    "dataNascimento": "01/04/1922",
    "endereco": {
        "logradouro": "RUA SEM ASFALTO",
        "numero": "S/N",
        "complemento": null,
        "bairro": "JARDIM BLA",
        "cidade": "PRESIDENTE PRUDENTE",
        "estado": "SP",
        "pais": "BRASIL"
    },
    "telefones": [
        "(18) 9283-2348"
    ],
    "emails": [
        "usuario@provedor.com.br"
    ],
    "status": "ATIVO"
}

## Remover logicamente um usuário
### [PUT] /users/{id}

## Obter usuários por nome, sobrenome e/ou cpf
### [GET] /users?nome=[NOME]&sobrenome=[SOBRENOME]&cpf=[CPF]