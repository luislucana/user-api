package br.com.api.user.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.api.user.entity.User;
import br.com.api.user.exception.BadRequestException;
import br.com.api.user.repository.UserRepository;
import br.com.api.user.request.UserRequest;
import br.com.api.user.response.UserResponse;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	
	@Mock
	UserRepository userRepository;
	
	@InjectMocks
	UserService userService  = new UserServiceProvider();
	
	@Test
	public void testCreateUserSuccess() throws BadRequestException {
		
		User user = new User();
		user.setId(99);
		
		Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
		
		
		UserRequest userRequest = new UserRequest();
		userRequest.setNome("Fulano");
		userRequest.setSobrenome("de Tal");
		
		UserResponse createdUser = userService.createUser(userRequest);
		
		Assert.assertEquals(user.getId(), createdUser.getId());
	}
}
