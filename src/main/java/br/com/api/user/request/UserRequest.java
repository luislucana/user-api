package br.com.api.user.request;

import java.util.List;

/**
 * Objeto que representa o JSON do usuario enviado pelo request.
 * 
 * @author Luis Lucana (luislucana@gmail.com)
 *
 */
public class UserRequest {
	
	private String nome;
	
	private String sobrenome;
	
	private String cpf;
	
	private String dataNascimento;
	
	private Endereco endereco;
	
	private List<String> telefones;
	
	private List<String> emails;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}
}
