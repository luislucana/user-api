package br.com.api.user.exception;

/**
 * Body do response retornado quando ocorrer UserNotFoundException.
 * 
 * @author Luis Lucana (luislucana@gmail.com)
 *
 */
public class UserNotFoundExceptionBody {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
