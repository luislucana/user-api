package br.com.api.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class UserApiExceptionHandler {

	@ExceptionHandler
	@ResponseBody
	public ResponseEntity<BadRequestExceptionBody> handleException(BadRequestException badRequestException, WebRequest request) {
		BadRequestExceptionBody bodyResponse = new BadRequestExceptionBody();
		bodyResponse.setErrorMessages(badRequestException.getErrorMessages());
		return new ResponseEntity<BadRequestExceptionBody>(bodyResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler
	@ResponseBody
	public ResponseEntity<UserNotFoundExceptionBody> handleException(UserNotFoundException userNotFoundException, WebRequest request) {
		UserNotFoundExceptionBody bodyResponse = new UserNotFoundExceptionBody();
		bodyResponse.setMessage(userNotFoundException.getMessage());
		return new ResponseEntity<UserNotFoundExceptionBody>(bodyResponse, HttpStatus.NOT_FOUND);
	}
}
