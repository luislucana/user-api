package br.com.api.user.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * Body do response retornado quando ocorrer BadRequestException.
 * 
 * @author Luis Lucana (luislucana@gmail.com)
 *
 */
public class BadRequestExceptionBody {

	List<String> errorMessages = new ArrayList<String>();

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
}
