package br.com.api.user.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * Exception para representar uma requisicao invalida.
 * 
 * @author Luis Lucana (luislucana@gmail.com)
 *
 */
@SuppressWarnings("serial")
public class BadRequestException extends Exception {
	
	private List<String> errorMessages = new ArrayList<String>();
	
	public BadRequestException(String message) {
		super(message);
		errorMessages.add(message);
	}
	
	private BadRequestException(List<String> messages) {
		super("Dados inválidos.");
		this.errorMessages = messages;
	}
	
	public static BadRequestException buildWithErrorMessages(final List<String> errorMessages) {
		final BadRequestException badRequestException = new BadRequestException(errorMessages);
		return badRequestException;
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}
}
