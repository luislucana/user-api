package br.com.api.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.api.user.exception.BadRequestException;
import br.com.api.user.exception.UserNotFoundException;
import br.com.api.user.request.UserRequest;
import br.com.api.user.response.UserResponse;
import br.com.api.user.service.UserService;

@Controller
@RequestMapping(path = "/users")
public class UserController {
	
	@Autowired
	UserService userService;

	/**
	 * Operation for add a new user.
	 * 
	 * @param userRequest
	 * @return a representation of the user created.
	 * @throws BadRequestException
	 */
	@PostMapping(path = "/add")
	public HttpEntity<UserResponse> addUser(@RequestBody final UserRequest userRequest) throws BadRequestException {
		UserResponse createdUser = userService.createUser(userRequest);
		return new ResponseEntity<UserResponse>(createdUser, HttpStatus.CREATED);
	}
	
	/**
	 * Operation for add a list of users.
	 * 
	 * @param usersRequest
	 * @return an iterable object containing all users created in this request.
	 * @throws BadRequestException
	 */
	@PostMapping(path = "/add/all")
	public HttpEntity<Iterable<UserResponse>> addAllUsers(@RequestBody final List<UserRequest> usersRequest) throws BadRequestException {
		Iterable<UserResponse> allUsers = userService.createUsers(usersRequest);
		return new ResponseEntity<Iterable<UserResponse>>(allUsers, HttpStatus.CREATED);
	}

	/**
	 * Returns a list of all users registered.
	 * 
	 * @return an iterable object containing all users registered.
	 */
	@GetMapping(path = "/all")
	public HttpEntity<Iterable<UserResponse>> getAllUsers() {
		Iterable<UserResponse> allUsers = userService.getAllUsers();
		return new ResponseEntity<Iterable<UserResponse>>(allUsers, HttpStatus.OK);
	}
	
	/**
	 * Updates the user data.
	 * 
	 * @param id
	 * @param userRequest
	 * @return the representation of the User updated.
	 * @throws UserNotFoundException
	 * @throws BadRequestException
	 */
	@PutMapping(path = "/{id}")
	public HttpEntity<UserResponse> updateUser(@PathVariable("id") final Integer id, @RequestBody final UserRequest userRequest) 
			throws UserNotFoundException, BadRequestException {
		UserResponse user = userService.updateUser(id, userRequest);
		return new ResponseEntity<UserResponse>(user, HttpStatus.OK);
	}
	
	/**
	 * Removes logically a user, given an identifier.
	 * 
	 * @param id The user's identifier
	 * @return a representation of the User inactivated
	 * @throws UserNotFoundException
	 * @throws BadRequestException
	 */
	@PutMapping(path = "/{id}/remove")
	public HttpEntity<UserResponse> removeUser(@PathVariable("id") final Integer id) throws UserNotFoundException, BadRequestException {
		UserResponse user = userService.inactivateUser(id);
		return new ResponseEntity<UserResponse>(user, HttpStatus.OK);
	}
	
	/**
	 * Returns a list of users that attends the informed conditions.
	 * 
	 * @param nome O primeiro nome do usuario.
	 * @param sobrenome Sobrenome do usuario.
	 * @param cpf Documento (CPF) do usuario.
	 * 
	 * @return A list of users that attends the informed conditions.
	 * 
	 * @throws BadRequestException if all parameters are not informed.
	 */
	@GetMapping
	public HttpEntity<Iterable<UserResponse>> getUsers(@RequestParam(value = "nome", required = false) String nome, 
			@RequestParam(value = "sobrenome", required = false) String sobrenome, @RequestParam(value = "cpf", required = false) String cpf) 
					throws BadRequestException {
		Iterable<UserResponse> users = userService.getUsers(nome, sobrenome, cpf);
		return new ResponseEntity<Iterable<UserResponse>>(users, HttpStatus.OK);
	}	
}
