package br.com.api.user.mapper;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import br.com.api.user.entity.Address;
import br.com.api.user.entity.User;
import br.com.api.user.request.Endereco;
import br.com.api.user.request.UserRequest;
import br.com.api.user.response.UserResponse;

/**
 * Mapper que converte os dados do request para os dados de entity para serem persistidos.
 * 
 * @author Luis Lucana (luislucana@gmail.com)
 *
 */
@Mapper
public interface UserMapper {

	UserMapper INSTANCE = Mappers.getMapper( UserMapper.class );
	
	@Named("userRequestToUser")
	@Mappings({
		@Mapping(target = "firstName", expression = "java( userRequest.getNome() != null ? userRequest.getNome().toUpperCase() : null )"),
		@Mapping(target = "lastName", expression = "java( userRequest.getSobrenome() != null ? userRequest.getSobrenome().toUpperCase() : null )"),
		@Mapping(source = "cpf", target = "document"),
		@Mapping(source = "dataNascimento", target = "birthDate", dateFormat = "dd/MM/yyyy"),
		@Mapping(source = "endereco", target = "address"),
		@Mapping(source = "telefones", target = "phoneNumbers"),
		@Mapping(target = "active", constant = "1")
	 })
    User userRequestToUser(UserRequest userRequest);
	
	@IterableMapping(qualifiedByName="userRequestToUser")
    Iterable<User> usersRequestToUsers(List<UserRequest> usersRequest);
	
	@Mappings({
		@Mapping(target = "address", expression = "java( endereco.getLogradouro() != null ? endereco.getLogradouro().toUpperCase() : null )"),
		@Mapping(source = "numero", target = "number"),
		@Mapping(target = "complement", expression = "java( endereco.getComplemento() != null ? endereco.getComplemento().toUpperCase() : null )"),
		@Mapping(target = "neighborhood", expression = "java( endereco.getBairro() != null ? endereco.getBairro().toUpperCase() : null )"),
		@Mapping(target = "city", expression = "java( endereco.getCidade() != null ? endereco.getCidade().toUpperCase() : null )"),
		@Mapping(target = "state", expression = "java( endereco.getEstado() != null ? endereco.getEstado().toUpperCase() : null )"),
		@Mapping(target = "country", expression = "java( endereco.getPais() != null ? endereco.getPais().toUpperCase() : null )"),
	 })
	Address enderecoToAddress(Endereco endereco);
	
	@Named("userToUserResponse")
	@Mappings({
		@Mapping(source = "firstName", target = "nome"),
		@Mapping(source = "lastName", target = "sobrenome"),
		@Mapping(source = "document", target = "cpf"),
		@Mapping(source = "birthDate", target = "dataNascimento", dateFormat = "dd/MM/yyyy"),
		@Mapping(source = "address", target = "endereco"),
		@Mapping(source = "phoneNumbers", target = "telefones"),
		@Mapping(target = "status", expression = "java( user.getActive() == 1 ? \"ATIVO\" : \"INATIVO\")")
	 })
    UserResponse userToUserResponse(User user);
	
	@IterableMapping(qualifiedByName="userToUserResponse")
    Iterable<UserResponse> usersToUsersResponse(Iterable<User> users);
	
	@Mappings({
		@Mapping(source = "address", target = "logradouro"),
		@Mapping(source = "number", target = "numero"),
		@Mapping(source = "complement", target = "complemento"),
		@Mapping(source = "neighborhood", target = "bairro"),
		@Mapping(source = "city", target = "cidade"),
		@Mapping(source = "state", target = "estado"),
		@Mapping(source = "country", target = "pais"),
	 })
	Endereco addressToEndereco(Address address);
}
