package br.com.api.user.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * User entity representation.
 * 
 * @author Luis Lucana (luislucana@gmail.com)
 *
 */
@Entity
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    
    @Column(name="firstName", nullable = false, length = 100)
	private String firstName;
	
    @Column(name="lastName", nullable = false, length = 100)
	private String lastName;
	
	@Column(name="document", nullable = false, unique=true)
	private String document;
	
	@Column(name="birthDate", nullable = false)
	private Date birthDate;
	
	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "address_id", nullable = false)
	private Address address;
	
	@ElementCollection
    @CollectionTable(name = "phone_number", joinColumns = @JoinColumn(name = "user_id"))
	@Column(name = "phone_number", nullable = false)
	private List<String> phoneNumbers;

	@ElementCollection
	@CollectionTable(name = "email", joinColumns = @JoinColumn(name = "user_id"))
	@Column(name = "email")
	private List<String> emails;
	
	@Min(0)
	@Max(1)
	@Column(name="active", nullable = false)
	private Integer active;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}
}
