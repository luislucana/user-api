package br.com.api.user.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Address {
	@Id
	@GeneratedValue
	private Integer id;

	@Column(name="address", nullable = false, length = 200)
	private String address;

	@Column(name="number", nullable = false, length = 10)
	private String number;

	@Column(name="complement", nullable = true, length = 30)
	private String complement;

	@Column(name="neighborhood", nullable = false, length = 50)
	private String neighborhood;

	@Column(name="city", nullable = false, length = 50)
	private String city;

	@Column(name="state", nullable = false, length = 2)
	private String state;

	@Column(name="country", nullable = false, length = 50)
	private String country;

	public long getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
