package br.com.api.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.api.user.entity.User;

/**
 * Repositorio da entidade User.
 * 
 * @author Luis Lucana (luislucana@gmail.com)
 *
 */
public interface UserRepository extends CrudRepository<User, Integer> {
	
	/**
	 * Consulta para retornar registros de User dado um valor de 'document'.
	 * 
	 * @param document
	 * @return
	 */
	//@Query("SELECT user FROM User user WHERE user.document = :document") 
	List<User> findByDocument(String document);
	
	@Query("SELECT user FROM User user WHERE user.firstName LIKE CONCAT('%',:firstName,'%')")
	List<User> findUserByFirstName(@Param("firstName") String firstName);
	
	@Query("SELECT user FROM User user WHERE user.lastName LIKE CONCAT('%',:lastName,'%')")
	List<User> findUserByLastName(@Param("lastName") String lastName);
	
	@Query("SELECT user FROM User user WHERE user.firstName LIKE CONCAT('%',:firstName,'%') AND " +
            "user.lastName LIKE CONCAT('%',:lastName, '%')")
	List<User> findUserByFirstNameLastName(@Param("firstName") String firstName, @Param("lastName") String lastName);
	
	@Query("SELECT user FROM User user WHERE user.firstName LIKE CONCAT('%',:firstName,'%') AND " +
            "user.lastName LIKE CONCAT('%',:lastName, '%') AND user.document = :document")
	List<User> findUserByFirstNameLastNameDocument(@Param("firstName") String firstName, @Param("lastName") String lastName, 
			@Param("document") String document);
	
	@Query("SELECT user FROM User user WHERE user.lastName LIKE CONCAT('%',:lastName, '%') AND user.document = :document")
	List<User> findUserByLastNameDocument(@Param("lastName") String lastName, @Param("document") String document);
	
	@Query("SELECT user FROM User user WHERE user.firstName LIKE CONCAT('%',:firstName,'%') AND user.document = :document")
	List<User> findUserByFirstNameDocument(@Param("firstName") String firstName, @Param("document") String document);
}
