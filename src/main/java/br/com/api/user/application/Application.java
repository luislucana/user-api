package br.com.api.user.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Classe principal da API. A API deve ser inicializada por esta classe
 * para ser executada. Nao ha necessidade de instalar na maquina um servidor de
 * aplicacao, ja que no Spring Boot, o Tomcat vem "embutido".
 * 
 * @author Luis Lucana (luislucana@gmail.com)
 *
 */
@SpringBootApplication
@ComponentScan("br.com.api.user.*")
@EntityScan("br.com.api.user.entity")
@EnableJpaRepositories("br.com.api.user.repository")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}