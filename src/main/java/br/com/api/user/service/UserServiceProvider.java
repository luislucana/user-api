package br.com.api.user.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.api.user.entity.User;
import br.com.api.user.exception.BadRequestException;
import br.com.api.user.exception.UserNotFoundException;
import br.com.api.user.mapper.UserMapper;
import br.com.api.user.repository.UserRepository;
import br.com.api.user.request.Endereco;
import br.com.api.user.request.UserRequest;
import br.com.api.user.response.UserResponse;

/**
 * Classe que contem a implementacao dos servicos.
 * 
 * @author Luis Lucana (luislucana@gmail.com)
 *
 */
@Service
public class UserServiceProvider implements UserService {
	
	private static final Integer YES = Integer.valueOf(1);
	private static final Integer NO = Integer.valueOf(0);
	
	private static final String PATTERN_DATE = "dd/MM/yyyy";
	
	@Autowired
	private UserRepository userRepository;
	
	/**
	 * Cria um usuario.
	 * 
	 * @param userRequest
	 * @return
	 * @throws BadRequestException
	 */
	@Override
	public UserResponse createUser(final UserRequest userRequest) throws BadRequestException {
		
		validateFields(userRequest);

		final User user = UserMapper.INSTANCE.userRequestToUser(userRequest);
		
		String cpf = user.getDocument();
		List<User> usersFound = userRepository.findByDocument(cpf);
		
		if (usersFound != null && !usersFound.isEmpty()) {
			throw new BadRequestException(String.format("Ja existe usuario cadastrado com o CPF '%s'.", cpf));
		}
		
		user.setActive(YES);
		
		final User savedUser = userRepository.save(user);
		
		UserResponse userResponse = UserMapper.INSTANCE.userToUserResponse(savedUser);
		
		return userResponse;
	}

	/**
	 * Cria mais de um usuario de uma vez.
	 * 
	 * @param usersRequest
	 * @return os usuarios criados.
	 * @throws BadRequestException
	 */
	@Override
	public Iterable<UserResponse> createUsers(final List<UserRequest> usersRequest) throws BadRequestException {
		
		validateFields(usersRequest);
		
		final Iterable<User> users = UserMapper.INSTANCE.usersRequestToUsers(usersRequest);
		
		Iterable<User> savedUsers = userRepository.saveAll(users);
		
		Iterable<UserResponse> usersResponse = UserMapper.INSTANCE.usersToUsersResponse(savedUsers);
		
		return usersResponse;
	}

	/**
	 * Obtem todos os usuarios cadastrados.
	 * 
	 * @return todos os usuarios cadastrados.
	 */
	@Override
	public Iterable<UserResponse> getAllUsers() {
		Iterable<User> allUsers = userRepository.findAll();
		
		Iterable<UserResponse> usersResponse = UserMapper.INSTANCE.usersToUsersResponse(allUsers);
		
		return usersResponse;
	}
	
	/**
	 * Obtem os usuarios de acordo com o nome, sobrenome e/ou cpf informados.
	 * 
	 * @param nome
	 * @param sobrenome
	 * @param cpf
	 * @return um conjunto de usuarios cadastrados que atendem aos criterios informados.
	 * @throws BadRequestException 
	 */
	@Override
	public Iterable<UserResponse> getUsers(String nome, String sobrenome, String cpf) throws BadRequestException {
		Iterable<UserResponse> usersResponse = null;
		List<User> users = null;
		
		nome = nome != null ? nome.trim().toUpperCase() : null;
		sobrenome = sobrenome != null ? sobrenome.trim().toUpperCase() : null;
		cpf = cpf != null ? cpf.trim() : null;
		
		if (StringUtils.isAllBlank(nome, sobrenome, cpf)) {
			throw new BadRequestException("Pelo menos 1 parâmetro deve ser informado para esta operação.");
		} 
		
		if (StringUtils.isNoneBlank(nome, sobrenome, cpf)) {
			
			users = userRepository.findUserByFirstNameLastNameDocument(nome, sobrenome, cpf);
			
		} else if (StringUtils.isNotBlank(nome) && StringUtils.isAllBlank(sobrenome, cpf)) {
			
			users = userRepository.findUserByFirstName(nome);
			
		} else if (StringUtils.isNotBlank(sobrenome) && StringUtils.isAllBlank(nome, cpf)) {
			
			users = userRepository.findUserByLastName(sobrenome);
			
		} else if (StringUtils.isNotBlank(cpf) && StringUtils.isAllBlank(nome, sobrenome)) {
			
			users = userRepository.findByDocument(cpf);
			
		} else if (StringUtils.isNoneBlank(nome, sobrenome) && StringUtils.isBlank(cpf)) {
			
			users = userRepository.findUserByFirstNameLastName(nome, sobrenome);
			
		} else if (StringUtils.isNoneBlank(nome, cpf) && StringUtils.isBlank(sobrenome)) {
			
			users = userRepository.findUserByFirstNameDocument(nome, cpf);
			
		} else if (StringUtils.isNoneBlank(sobrenome, cpf) && StringUtils.isBlank(nome)) {
			
			users = userRepository.findUserByLastNameDocument(sobrenome, cpf);
			
		}
		
		usersResponse = UserMapper.INSTANCE.usersToUsersResponse(users);
		
		return usersResponse;
	}

	/**
	 * Atualiza os dados de um usuario.
	 * 
	 * @param id
	 * @param userRequest
	 * @return o usuario com os dados alterados.
	 * @throws UserNotFoundException
	 * @throws BadRequestException
	 */
	@Override
	public UserResponse updateUser(final Integer id, final UserRequest userRequest) throws UserNotFoundException, BadRequestException {
		
		validateFields(userRequest);
		
		Optional<User> optionalUser = userRepository.findById(id);
		if (!optionalUser.isPresent()) {
			throw new UserNotFoundException("Usuario nao encontrado.");
		}
		
		User retrievedUser = optionalUser.get();
		
		if (retrievedUser.getActive().equals(NO)) {
			throw new BadRequestException("Usuario ja se encontra inativo.");
		}
		
		final User user = UserMapper.INSTANCE.userRequestToUser(userRequest);
		
		User savedUser = userRepository.save(user);
		
		UserResponse userResponse = UserMapper.INSTANCE.userToUserResponse(savedUser);
		
		return userResponse;
	}

	/**
	 * Inativa um usuario.
	 * 
	 * @param userId
	 * @return
	 * @throws UserNotFoundException
	 * @throws BadRequestException
	 */
	@Override
	public UserResponse inactivateUser(final Integer userId) throws UserNotFoundException, BadRequestException {
		
		Optional<User> optionalUser = userRepository.findById(userId);
		if (!optionalUser.isPresent()) {
			throw new UserNotFoundException("Usuario nao encontrado.");
		}
		
		User user = optionalUser.get();
		
		if (user.getActive().equals(NO)) {
			throw new BadRequestException("Usuario ja se encontra inativo.");
		}
		
		user.setActive(NO);
		
		User savedUser = userRepository.save(user);
		
		UserResponse userResponse = UserMapper.INSTANCE.userToUserResponse(savedUser);
		
		return userResponse;
	}

	/**
	 * Metodo para validar os campos da UserRequest.
	 * 
	 * @param userRequest
	 * @throws BadRequestException caso um ou mais campos nao tenham sido informados corretamente.
	 */
	private void validateFields(final UserRequest userRequest) throws BadRequestException {
		List<String> errorMessages = new ArrayList<String>();
		
		if (StringUtils.isBlank(userRequest.getNome())) {
			errorMessages.add("O campo 'nome' é obrigatório.");
		}
		if (StringUtils.isBlank(userRequest.getSobrenome())) {
			errorMessages.add("O campo 'sobrenome' é obrigatório.");		
		}
		if (StringUtils.isBlank(userRequest.getCpf())) {
			errorMessages.add("O campo 'cpf' é obrigatório.");
		} else if (!StringUtils.isNumeric(userRequest.getCpf())) {
			errorMessages.add("O campo 'cpf' deve conter somente números.");
		}
		if (StringUtils.isBlank(userRequest.getDataNascimento())) {
			errorMessages.add("O campo 'dataNascimento' é obrigatório.");
		} else {
			String dataNascimento = userRequest.getDataNascimento();
			
			try {
				new SimpleDateFormat(PATTERN_DATE).parse(dataNascimento);
			} catch (ParseException parseException) {
				errorMessages.add("O campo 'dataNascimento' deve ser informado no formato DD/MM/AAAA.");
			}
		}
		
		if (userRequest.getEndereco() == null) {
			errorMessages.add("O campo 'endereco' é obrigatório.");
		} else {
			Endereco endereco = userRequest.getEndereco();
			
			String logradouro = endereco.getLogradouro();
			String numero = endereco.getNumero();
			String bairro = endereco.getBairro();
			String cidade = endereco.getCidade();
			String estado = endereco.getEstado();
			String pais = endereco.getPais();
			
			if (StringUtils.isBlank(logradouro)) {
				errorMessages.add("O campo 'endereco.logradouro' é obrigatório.");	
			}
			if (StringUtils.isBlank(numero)) {
				errorMessages.add("O campo 'endereco.numero' é obrigatório.");	
			}
			if (StringUtils.isBlank(bairro)) {
				errorMessages.add("O campo 'endereco.bairro' é obrigatório.");	
			}
			if (StringUtils.isBlank(cidade)) {
				errorMessages.add("O campo 'endereco.cidade' é obrigatório.");	
			}
			if (StringUtils.isBlank(estado)) {
				errorMessages.add("O campo 'endereco.estado' é obrigatório.");	
			}
			if (StringUtils.isBlank(pais)) {
				errorMessages.add("O campo 'endereco.pais' é obrigatório.");	
			}
		}
		
		if (userRequest.getTelefones() == null || userRequest.getTelefones().isEmpty()) {
			errorMessages.add("O campo 'telefones' é obrigatório.");
		}
		if (userRequest.getEmails() == null || userRequest.getEmails().isEmpty()) {
			errorMessages.add("O campo 'emails' é obrigatório.");
		}
		
		if (!errorMessages.isEmpty()) {
			BadRequestException badRequestException = BadRequestException.buildWithErrorMessages(errorMessages);
			throw badRequestException;
		}
	}
	
	/**
	 * Metodo para validar os campos da UserRequest.
	 * 
	 * @param usersRequest
	 * @throws BadRequestException caso um ou mais campos nao tenham sido informados corretamente.
	 */
	private void validateFields(final Iterable<UserRequest> usersRequest) throws BadRequestException {
		for (UserRequest userRequest : usersRequest) {
			validateFields(userRequest);
		}
	}
}
