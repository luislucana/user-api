package br.com.api.user.service;

import java.util.List;

import br.com.api.user.exception.BadRequestException;
import br.com.api.user.exception.UserNotFoundException;
import br.com.api.user.request.UserRequest;
import br.com.api.user.response.UserResponse;

/**
 * Interface da camada de servico.
 * 
 * @author Luis Lucana (luislucana@gmail.com)
 *
 */
public interface UserService {
	
	/**
	 * Cria um usuario.
	 * 
	 * @param userRequest
	 * @return
	 * @throws BadRequestException
	 */
	UserResponse createUser(final UserRequest userRequest) throws BadRequestException;
	
	/**
	 * Cria mais de um usuario de uma vez.
	 * 
	 * @param usersRequest
	 * @return os usuarios criados.
	 * @throws BadRequestException
	 */
	Iterable<UserResponse> createUsers(final List<UserRequest> usersRequest) throws BadRequestException;
	
	/**
	 * Obtem todos os usuarios cadastrados.
	 * 
	 * @return todos os usuarios cadastrados.
	 */
	Iterable<UserResponse> getAllUsers();
	
	/**
	 * Obtem os usuarios de acordo com o nome, sobrenome e/ou cpf informados.
	 * 
	 * @param nome
	 * @param sobrenome
	 * @param cpf
	 * @return um conjunto de usuarios cadastrados que atendem aos criterios informados.
	 * @throws BadRequestException 
	 */
	public Iterable<UserResponse> getUsers(String nome, String sobrenome, String cpf) throws BadRequestException;
	
	/**
	 * Atualiza os dados de um usuario.
	 * 
	 * @param id
	 * @param userRequest
	 * @return o usuario com os dados alterados.
	 * @throws UserNotFoundException
	 * @throws BadRequestException
	 */
	UserResponse updateUser(final Integer id, final UserRequest userRequest) throws UserNotFoundException, BadRequestException;
	
	/**
	 * Inativa um usuario.
	 * 
	 * @param userId
	 * @return
	 * @throws UserNotFoundException
	 * @throws BadRequestException
	 */
	UserResponse inactivateUser(final Integer userId) throws UserNotFoundException, BadRequestException;
}
